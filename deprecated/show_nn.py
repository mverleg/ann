
'''
	specific for 1-2-2 network
'''
def show_nn(weights_in_hidden, weights_hidden_out, bias_hidden, bias_out, activation_inp, activation_hidden, activation_outp, hidden_error = None, output_error = None):
	
	print '\nnetwork:'
	print '%+.2f                  %+.2f' % (activation_inp[0], activation_inp[1])
	print '  |  \              /     |  '
	print '%+.2f  \%+.2f    /%+.2f %+.2f' % (weights_in_hidden[0, 0], weights_in_hidden[1, 0], weights_in_hidden[0, 1], weights_in_hidden[1, 1])
	print '  |         \  /          |  '
	print '%+.2f%+.2f              %+.2f%+.2f' % (activation_hidden[0], bias_hidden[0], activation_hidden[1], bias_hidden[1])
	if not hidden_error is None:
		print '[%+.2f]               [%+.2f]' % (hidden_error[0], hidden_error[1])
	print '  |                 /'
	print '%+.2f          /%+.2f' % (weights_hidden_out[0][0], weights_hidden_out[0][1])
	print '  |      /'
	print '%+.2f%+.2f' % (activation_outp[0], bias_out[0])
	if not output_error is None:
		print '[%+.2f]' % (output_error[0])
	print '\n'
