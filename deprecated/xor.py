
'''
	with great help from 
	http://neuralnetworksanddeeplearning.com/chap2.html
'''

from matplotlib.pyplot import subplots, show
from numpy import array, dot, vectorize
from numpy.random import random

from deprecated import show_nn


inputs = array([
	[0., 0.],
	[0., 1.],
	[1., 0.],
	[1., 1.],
])
outputs = array([float(bool(a) and bool(b)) for a, b in inputs])

'''  settings '''
hidden_size = 2
#activation_function = lambda x: tanh(x) # ~sigmoid
#activation_derivative = lambda x: 4 * cosh(x)**2 / (cosh(2 * x) + 1)**2 # thanks, Wolfram
activation_function = vectorize(lambda x: min(max(x, -1.), +1.))
activation_derivative = vectorize(lambda x: 1. if -1 <= x <= 1 else 0.)
cost_function = lambda x: .5 * x**2
cost_derivative = lambda x: x
learning_rate = 0.1
error_treshold = .001
iterations_treshold = 200
show_output = False

''' random initialization '''
weights_in_hidden = random((hidden_size, len(inputs[0]))) * 2 - 1
weights_hidden_out = random((1, hidden_size)) * 2 - 1
bias_hidden = random((hidden_size,)) * 2 - 1
bias_out = random((1,)) * 2 - 1

err, iter, errors = 1., 0, []
while err > error_treshold and iter < iterations_treshold:
	
	err = 0.
	iter += 1
	
	for inp, outp in zip(inputs, outputs):
		
		''' forward propagation of the signal through the network '''
		signal_hidden = bias_hidden + dot(weights_in_hidden, inp)
		activation_hidden = activation_function(signal_hidden)
		signal_outp = bias_out + dot(weights_hidden_out, activation_hidden)
		activation_outp = activation_function(signal_outp)
		
		err += cost_function(activation_outp - outp)
		if show_output:
			print '%s : %+.3f  %+d' % ('  '.join('%+d' % i for i in inp), activation_outp, outp)
		
		if show_output:
			show_nn(weights_in_hidden, weights_hidden_out, bias_hidden, bias_out, inp, activation_hidden, activation_outp)
		
		''' BP1: error in output layer = d cost / d signal * derivative of activation (influence of signal * signal) '''
		output_error = cost_derivative(activation_outp - outp) * activation_derivative(signal_outp)
		
		''' BP2: use transpose of weights to propagate backwards, and apply it to the error, then 
			multiply it with the activation (small activation caused a small error) 
			this would normally be done for each layer in the network '''
		hidden_error = dot(weights_hidden_out.T, output_error) * activation_derivative(signal_hidden)
		
		''' BP3: the cost increase per bias step is equal to error '''
		dd_cost_bias_hidden = hidden_error
		dd_cost_bias_output = output_error
		
		''' BP4: the cost increase per weight step for connection a -> b is 
			the incomming activity from a * error in b (learn slowly if not actively contributing) '''
		dd_cost_weight_hidden = dot(inp[:, None], hidden_error[None, :])
		dd_cost_weight_output = dot(activation_hidden[:, None], output_error[None, :])
		
		''' update weights and biasses based on gradient in cost '''
		weights_in_hidden -= learning_rate * dd_cost_weight_hidden.T
		weights_hidden_out -= learning_rate * dd_cost_weight_output.T
		bias_hidden -= learning_rate * dd_cost_bias_hidden
		bias_out -= learning_rate * dd_cost_bias_output
		
		if show_output:
			show_nn(weights_in_hidden, weights_hidden_out, bias_hidden, bias_out, inp, activation_hidden, activation_outp, hidden_error, output_error)
	
	errors.append(err)
	if show_output or True:
		print '%4d err: %.4f' % (iter, err)

fig, ax = subplots()
ax.scatter(range(iter), errors)
ax.set_ylim([0, 2])
show()


