
from numpy import tanh, cosh, vectorize
from numpy import array
from marknet import BaseANN

'''
	training data
	XOR now; AND and OR should be easier
'''
X = array([
	[0., 0.], 
	[0., 1.], 
	[1., 0.], 
	[1., 1.]
])
Y = array([[float(bool(x1) ^ bool(x2))] for x1, x2 in X])

'''
	compare 3 networks
'''
tanhnet = BaseANN(
	layers = [2, 2, 1],
	activation_function = lambda x: tanh(x), # ~sigmoid
	activation_derivative = lambda x: 4 * cosh(x)**2 / (cosh(2 * x) + 1)**2, # thanks, Wolfram
	cost_function = lambda x: sum(n**2 for n in x), 
	cost_derivative = lambda x: x, 
	learning_rate = 0.1
)
tanhnet.optimize(X, Y, iterations = 10)
print 'with a sigmoid (tanh) activation function, the network can converge but gets stuck a lot'
print 'the one demonstrated in the presentation was similar to this one (1 xor 1 needs a capped activation function)\n'

linnet = BaseANN(
	layers = [2, 2, 1],
	activation_function = vectorize(lambda x: x),
	activation_derivative = vectorize(lambda x: 1),
	cost_function = lambda x: sum(n**2 for n in x), 
	cost_derivative = lambda x: x, 
)
linnet.optimize(X, Y, iterations = 10)
print 'with an identity activation function, the XOR gate cannot be expressed with just 2 hidden neurons\n'

biglinnet = BaseANN(
	layers = [2, 3, 1],
	activation_function = vectorize(lambda x: min(max(x, -1.), +1.)),
	activation_derivative = vectorize(lambda x: 1. if -1 <= x <= 1 else 0.),
	cost_function = lambda x: sum(n**2 for n in x), 
	cost_derivative = lambda x: x, 
)
biglinnet.optimize(X, Y, iterations = 10)
print 'with 3 neurons (and identity activation function), the XOR gate can be expressed! converges better than tanh\n'


