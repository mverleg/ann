
from matplotlib.pyplot import subplots, show
from numpy import tanh, cosh
from numpy import array
from marknet import BaseANN

'''
	training data
	XOR now; AND and OR should be easier
'''
X = array([
	[0., 0.], 
	[0., 1.], 
	[1., 0.], 
	[1., 1.]
])
Y = array([[float(bool(x1) ^ bool(x2))] for x1, x2 in X])
labels = ['%d xor %d' % (x1, x2) for x1, x2 in X]

'''
	see outputs converge (or diverge)
'''
net = BaseANN(
	layers = [2, 3, 1],
	activation_function = lambda x: tanh(x),
	activation_derivative = lambda x: 4 * cosh(x)**2 / (cosh(2 * x) + 1)**2,
	cost_function = lambda x: sum(n**2 for n in x), 
	cost_derivative = lambda x: x, 
	learning_rate = 0.05,  
	iterations_treshold = 10,
)
predictions = [net.think(X)[:, 0]]
for k in range(100):
	net.learn(X, Y)
	predictions.append(net.think(X)[:, 0])

predictions = array(predictions)
fig, ax = subplots()
for k in range(4):
	ax.plot(predictions[:, k], label = labels[k])
ax.legend(loc = 'lower right')
ax.set_xlabel('10 x steps (one steps trains all data once)')
ax.set_ylabel('output neuron value')
show()

print 'notice how the start is random, but the training usually converges (partially because the network is redundantly large)'


