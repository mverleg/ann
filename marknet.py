
'''
	with great help from 
	http://neuralnetworksanddeeplearning.com/chap2.html
'''

from matplotlib.pyplot import subplots
from numpy.random import random
from numpy import save, load, finfo, dot, array
from os import path, makedirs
from misc import mkdir_p


class BaseANN(object):
	
	def __init__(self, layers, activation_function, activation_derivative, cost_function, cost_derivative, learning_rate = 0.1, error_treshold = .00001, iterations_treshold = 1000):
		self.reset(layers)
		self.activation_function = activation_function
		self.activation_derivative = activation_derivative
		self.cost_function = cost_function
		self.cost_derivative = cost_derivative
		self.learning_rate = learning_rate
		self.error_treshold = error_treshold
		self.iterations_treshold = iterations_treshold
	
	def reset(self, layers = None):
		if layers is None:
			if not hasattr(self, 'biasses'):
				raise Exception('provide the number of neurons per layer when calling reset on a network that doesn\'t have weights yet')
			layers = [self.weights[0].shape[-1]] + [len(bias) for bias in self.biasses]
		prev_size = layers.pop(0)
		self.biasses, self.weights, = [], []
		for layer_size in layers:
			self.biasses.append(random((layer_size,)) * 2 - 1)
			self.weights.append(random((layer_size, prev_size)) * 2 - 1)
			prev_size = layer_size
	
	'''
		learn repeatedly; select outcome with lowest error
	'''
	def optimize(self, X, Y, iterations = 5, plot_errors = False, **kwargs):
		best_error, error_lists = finfo('d').max, []
		try:
			for k in range(iterations):
				errors = self.learn(X, Y, **kwargs)
				error_lists.append(errors)
				print 'NN#%d error %+4f' % (k + 1, errors[-1])
				if errors[-1] < best_error:
					best_biasses, best_weights, best_error = self.biasses, self.weights, errors[-1]
				if errors[-1] < self.error_treshold:
					break
				self.reset()
			if k == 0:
				print 'you can terminate optimization and select the best result so far with a keyboard interrupt (ctrl+C?)'
		except KeyboardInterrupt:
			if k == 0:
				print 'no training completed yet'
				exit()
			print 'optimization termined; best result so far will be used'
		self.biasses, self.weights = best_biasses, best_weights
		print 'lowest error: %+4f\n' % best_error
		if plot_errors:
			fig, ax = subplots()
			for k, errors in enumerate(error_lists):
				ax.scatter(range(len(errors)), errors, label = 'NN#%d' % k)
			ax.set_ylim([0, 2])
			ax.legend()
	
	'''
		push training data through the network and back-propagate
	'''
	def learn(self, X, Y, plot_error = False):
		
		err, errors = 1., []
		while err > self.error_treshold and len(errors) < self.iterations_treshold:
			
			err = 0.
			for inp, outp in zip(X, Y):
				
				''' forward propagation of the signal through the network '''
				signals, activations = self._propagate(inp)
				err += self.cost_function(activations[-1] - outp)
				
				''' calculate the gradient '''
				dd_cost_bias, dd_cost_weight = self._back_propagate(activations, signals, activations[-1] - outp)
				
				''' update weights and biasses based on gradient in cost '''
				for k, (dd_bias, dd_weight) in enumerate(zip(dd_cost_bias, dd_cost_weight)):
					self.biasses[k] -= self.learning_rate * dd_bias.T
					self.weights[k] -= self.learning_rate * dd_weight.T
			
			#print '%.4d: %+.3f' % (len(errors), err / len(Y))
			errors.append(err / len(Y))
		
		if plot_error:
			fig, ax = subplots()
			ax.scatter(range(len(errors)), errors)
			ax.set_ylim([0, 2])
		
		return errors
	
	def _propagate(self, X):
		''' layer by layer, propagate the signal forward, keeping both the activated and unactivated value '''
		signals, activations = [X], [X]
		for biasses, weights in zip(self.biasses, self.weights):
			signals.append(biasses + dot(weights, activations[-1]))
			activations.append(self.activation_function(signals[-1]))
		return signals, activations
	
	def _back_propagate(self, activations, signals, err):
		
		layer_errors = [None] * len(self.biasses)
		
		''' BP1: error in output layer = d cost / d signal * derivative of activation (influence of signal * signal) '''
		layer_errors[-1] = self.cost_derivative(err) * self.activation_derivative(signals[-1])
		
		for k, weights in enumerate(self.weights[::-1]):
			''' BP2: use transpose of weights to propagate backwards, and apply it to the error, then 
				multiply it with the activation (small activation caused a small error) 
				this would normally be done for each layer in the network '''
			if k == len(self.biasses) - 1:
				break # must be a much better way with array indexing
			layer_errors[-k-2] = dot(weights.T, layer_errors[-k-1]) * self.activation_derivative(signals[-k-2])
		
		''' BP3: the cost increase per bias step is equal to error '''
		dd_cost_bias = layer_errors
		
		''' BP4: the cost increase per weight step for connection a -> b is 
			the incomming activity from a * error in b (learn slowly if not actively contributing) '''
		dd_cost_weight = [dot(activations[k][:, None], layer_error[None, :]) for k, layer_error in enumerate(layer_errors)]
		
		return dd_cost_bias, dd_cost_weight
	
	'''
		use the network to predict an output
	'''
	def think(self, X):
		Y = []
		for x in X:
			Y.append(self._propagate(x)[1][-1])
		return array(Y)
	
	'''
		this just stores the current neurons in the network; it doesn't save things
		like activation functions etc [which should be okay if you call it on a trained NN]
	'''
	def save(self, dirname):
		mkdir_p(dirname)
		save(path.join(dirname, 'biasses.npy'), self.biasses)
		save(path.join(dirname, 'weights.npy'), self.weights)
	
	def load(self, dirname):
		self.biasses = load(path.join(dirname, 'biasses.npy'))
		self.weights = load(path.join(dirname, 'weights.npy'))

if __name__ == '__main__':
	print 'run one of the other files for a demo of this class'


