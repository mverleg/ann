
from os import makedirs
from os.path import isdir
import errno


'''
	mkdir -p   python should have this
	http://stackoverflow.com/questions/600268/mkdir-p-functionality-in-python
'''
def mkdir_p(path):
    try:
        makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and isdir(path):
            pass
        else: raise


