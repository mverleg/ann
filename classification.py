
from matplotlib.colors import ListedColormap, BoundaryNorm
from matplotlib.pyplot import subplots, show
from numpy import linspace, empty, tanh, cosh, array, vectorize
from numpy.random import random, randint
from marknet import BaseANN


print 'this demo will try to classify points in a 2D, non-linear pattern\nit takes a while to run (minutes)'

def get_class(x, y):
	if 2**2 < x**2 + y**2 < 4.5**2:
		return 1
	if x + y < 0:
		return 2
	return 3

N_back, N_train, N_test = 51, 700, 1000
L, N = 6., 21

'''
	create training data
'''
xs = linspace(+L, -L, N_back)
ys = linspace(-L, +L, N_back)
M = empty((N_back, N_back))
for m, x in enumerate(xs):
	for n, y in enumerate(ys):
		M[m, n] = get_class(x, y)

X_train = random((1 * N_train, 2)) * 2 * L - L
Y_train = empty((1 * N_train, 3))
for k, (x, y) in enumerate(X_train):
	q = array([0., 0., 0.])
	q[get_class(x, y) - 1] = 1.
	Y_train[k] = q

print 'classification often works with fewer neurons if the output is of the form: [probability for class 1, probability for class 2, etc.]'

'''
	plotting stuff (not displayed until "show()"
'''
fig, (ax_back, ax_true, ax_fit) = subplots(1, 3, figsize = (17, 5))
cmap = ListedColormap([[1, 0, 0, .5], [0, 1, 0, .5], [0, 0, 1, .5]])
ax_back.imshow(M, interpolation = 'none', cmap = cmap, norm = BoundaryNorm((.5, 1.5, 2.5, 3.5), cmap.N))
#ax_fit.imshow(M, interpolation = 'none', extent = [-L, +L, +L, -L], cmap = cmap, norm = BoundaryNorm((.5, 1.5, 2.5, 3.5), cmap.N))
ax_true.scatter(X_train[:, 0], X_train[:, 1], color = Y_train)
ax_true.set_xlim([-L, +L])
ax_true.set_ylim([-L, +L])
ax_true.set_title('TRUE')
ax_fit.set_xlim([-L, +L])
ax_fit.set_ylim([-L, +L])
ax_fit.set_title('FIT')

'''
	training
'''
net = BaseANN(
	layers = [2, 70, 60, 50, 3],
	activation_function = vectorize(lambda x: -1 if x < -1 else (+1 if x > +1 else x)),
	activation_derivative = vectorize(lambda x: 1. if -1 < x < 1 else 0.),
	cost_function = lambda x: sum(n**2 for n in x), 
	cost_derivative = lambda x: x, 
	learning_rate = 0.01,
	iterations_treshold = 300,
)

try:
	net.load('bestnet')
	print 'loaded from cache'
except IOError:
	print 'optimizing network'
	net.optimize(X_train, Y_train, iterations = 5, plot_error = True)
	net.save('bestnet')

'''
	testing
'''
print 'predicting'
X_test = random((1 * N_test, 2)) * 2 * L - L
Y_test = net.think(X_test)
for k, Y_row in enumerate(Y_test):
	n = Y_row.argmax()
	Y_test[k] = array([0., 0., 0.])
	Y_test[k][n] = 1.

'''
	validation
'''
predicted_classes = [yt.argmax() for yt in Y_test]
true_classes = [get_class(x, y) for x, y in X_test]

'''
	show results
'''
ax_fit.scatter(X_test[:, 0], X_test[:, 1], color = Y_test)
show()

