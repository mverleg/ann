# Convergence of XOR predictions #

![convergence image](/mverleg/ann/raw/dfd106d0a56d3948bd2c51108161d9c935502ac3/images/xor_convergence.png)

# Classifier #

![classifier (easy)](/mverleg/ann/raw/e58851b7ed7005da06707eb5b44324c17f626690/images/nn_classifier.png)

